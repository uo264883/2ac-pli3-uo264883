
    .data

string1:       .asciiz "whatever1"   ; first string
string2:       .asciiz "whatever2"   ; second string


    .code
main:
    xor    r16, r16, r16   ; letter count
    xor    r17, r17, r17   ; index to current item
loop:
    lbu    r4, string1(r17) ; load next digit (ascii code)
    beqz   r4, next         ; end of string?
    daddui   r16, r16, 1    ; update letter count
    daddui r17, r17, 1     ; move to next index
    j      loop
next:
    xor    r15, r15, r15   ; letter count
    xor    r17, r17, r17   ; index to current item
loop2:
    lbu    r4, string2(r17) ; load next digit (ascii code)
    beqz   r4, compare         ; end of string?
    daddui   r15, r15, 1    ; update letter count
    daddui r17, r17, 1     ; move to next index
    j      loop2
compare:
    xor r8, r8, r8
    beq r15, r16, equal
    j end
equal:
    daddui r8, r8, 1 ; r8 contains 0 if words do not have the same length, 1 otherwise
end:
    halt
