
    .data

string:       .asciiz "whatever"
result:        .word 0

    .code
main:
    xor    r16, r16, r16   ; letter count
    xor    r17, r17, r17   ; index to current item
loop:
    lbu    r4, string(r17) ; load next digit (ascii code)
    beqz   r4, end         ; end of string?
    daddui   r16, r16, 1    ; update letter count
    daddui r17, r17, 1     ; move to next index
    j      loop
end:
    sd     r16, result(r0) ; save result
    halt
